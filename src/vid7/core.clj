(ns vid7.core
  (:require [crux.api :as crux]))

(def node (crux/start-node {}))

(def bmw {:crux.db/id :bmw
          :make/name "BMW"
          :make/country "Germany"})

(def opel {:crux.db/id :opel
           :make/name "Opel"
           :make/country "Germany"})

(def car1 {:crux.db/id :car1
           :car/year 2015
           :car/make :bmw
           :car/model "3 Series"
           :car/variant "135i"})

(def car2 {:crux.db/id :car2
           :car/year 2013
           :car/make :opel
           :car/model "Corsa"
           :car/variant "Cosmo"})

(def fiat {:crux.db/id :fiat
           :make/name "Fiat"
           :make/country "Italy"})

(def car3 {:crux.db/id :car3
           :car/year 2010
           :car/make :fiat
           :car/model "500"
           :car/variant "1.2"})

(def thing {:crux.db/id :thing1
            :thing/time (java.time.Instant/now)})

(def things (map (fn [e] [:crux.tx/put e]) [car1 car2 car3 bmw opel fiat]))

(comment

  (crux/submit-tx node things)
  (crux/submit-tx node [[:crux.tx/put thing]])

  (crux/q (crux/db node)
          '{:find [(pull ?car [* {:car/year [*]}])]
            :where [[?make :crux.db/id :bmw]
                    [?car :car/make ?make]]}) 

  (crux/q (crux/db node)
          '{:find [?time]
            :where [[?thing :thing/time]
                    [?thing :thing/time ?time]]})

  (crux/q (crux/db node)
          '{:find [?year]
            :where [[?year :car/year]]})


  )
